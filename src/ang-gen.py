# import modules
import sys
import os
import argparse
import subprocess
import re
import json
import urwid
import wget
import shutil

version = '1.0.2'
currentDir = os.getcwd()
directory = ''
appDir = ''
componentsDir = ''
projectName = ''
scriptsDir = ''
serviceDir = ''
factoriesDir = ''
valuesDir = ''
scriptsPath = ''
directivesDir = ''
projectExist = 'false'
indexScripts = []
indexScripts.clear()
routableComponents = []
basePath = ''


def fetchDeps():
    angularJsUrl = "https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"
    uiRouterUrl = "https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/1.0.18/angular-ui-router.js"
    jQueryUrl = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"
    assets = os.path.join(appDir, r'assets')
    os.makedirs(assets)
    wget.download(angularJsUrl)
    angPath = os.path.join(assets, r'angular.js')
    wget.download(uiRouterUrl)
    uiPath = os.path.join(assets, r'ui-router.js')
    wget.download(jQueryUrl)
    jQueryPath = os.path.join(assets, r'jQuery.js')
    shutil.move('./angular.js', angPath)
    shutil.move('./angular-ui-router.js', uiPath)
    shutil.move('./jquery.js', jQueryPath)


def generateIndex(path, name):
    global indexScripts
    scriptsJson = os.path.join(directory, r'scripts.json')
    # need to prepend the script info from scriptsJson to the indexscripts without dupes
    if os.path.isfile(scriptsJson):
        #print('Hey scriptsJson found!')
        with open(scriptsJson, 'r') as f:
            datastore = json.load(f)
        # print(datastore)
    scripts2 = []
    scripts3 = ''
    scripts2.clear()
    scriptName = name + '.js'
    global basePath
    if(projectExist == 'true'):
        config = os.path.join(directory, r'config.json')
        with open(config, 'r') as f:
            dataconfig = json.load(f)
        print(dataconfig)
    else:
        dataconfig = dict([('basePath', ''), ('other', 1)])
    if(basePath != ''):
        scriptPath = basePath + '/app/' + path + '/' + name + '/' + scriptName
    elif(dataconfig["basePath"] != ''):
        scriptPath = dataconfig["basePath"] + \
            '/app/' + path + '/' + name + '/' + scriptName
        basePath = dataconfig["basePath"]
    else:
        scriptPath = './app/' + path + '/' + name + '/' + scriptName
    print("Base Path: ", basePath)
    scripts = """    
  <script src='{}'></script>
  """.format(scriptPath)
    if(projectExist == 'true'):
        for i in datastore:
            indexScripts.append(i)
    indexScripts.append(scripts)
    scripts3 = ''
    indexScriptsNoDupes = list(set(indexScripts))
    for i in indexScriptsNoDupes:
        scripts3 = scripts3 + i
    with open(scriptsJson, 'w') as outfile:
        json.dump(indexScriptsNoDupes, outfile)
    indexPath = os.path.join(directory, r'index.html')
    indexHtml = open(indexPath, 'w')
    indexFile = '''<!DOCTYPE html>
<html lang="en"> <!-- DO NOT EDIT THIS FILE!! THIS FILE IS MANAGED BY THE CLI! -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body ng-app="{}" >
    <main></main>
    <!--Default Scripts-->
    <script src="{}/app/assets/angular.js"></script>
    <script src="{}/app/assets/ui-router.js"></script>
    <script src="{}/app/assets/jquery.js"></script>
    <script src="{}/app/scripts/app.js"></script>
    <script src="{}/app/scripts/routerConfig.js"></script>
    <!--Injectable scripts-->
    <!--Inject Start-->
    {}
    <!--Inject end--> 
</body>
</html>'''.format(projectName, basePath, basePath, basePath, basePath, basePath, scripts3)
    indexHtml.write(indexFile)
    indexHtml.close()

def generateNetIndex(path, name):
    global indexScripts
    scriptsJson = os.path.join(directory, r'scripts.json')
    # need to prepend the script info from scriptsJson to the indexscripts without dupes
    if os.path.isfile(scriptsJson):
        #print('Hey scriptsJson found!')
        with open(scriptsJson, 'r') as f:
            datastore = json.load(f)
        # print(datastore)
    scripts2 = []
    scripts3 = ''
    scripts2.clear()
    scriptName = name + '.js'
    global basePath
    if(projectExist == 'true'):
        config = os.path.join(directory, r'config.json')
        with open(config, 'r') as f:
            dataconfig = json.load(f)
        print(dataconfig)
    else:
        dataconfig = dict([('basePath', ''), ('other', 1)])
    if(basePath != ''):
        scriptPath = basePath + '/app/' + path + '/' + name + '/' + scriptName
    elif(dataconfig["basePath"] != ''):
        scriptPath = dataconfig["basePath"] + \
            '/app/' + path + '/' + name + '/' + scriptName
        basePath = dataconfig["basePath"]
    else:
        scriptPath = './app/' + path + '/' + name + '/' + scriptName
    print("Base Path: ", basePath)
    scripts = """    
  <script src='{}'></script>
  """.format(scriptPath)
    if(projectExist == 'true'):
        for i in datastore:
            indexScripts.append(i)
    indexScripts.append(scripts)
    scripts3 = ''
    indexScriptsNoDupes = list(set(indexScripts))
    for i in indexScriptsNoDupes:
        scripts3 = scripts3 + i
    with open(scriptsJson, 'w') as outfile:
        json.dump(indexScriptsNoDupes, outfile)
    indexPath = os.path.join(directory, r'index.html')
    indexHtml = open(indexPath, 'w')
    indexFile = '''@{
    ViewBag.Title = "App";
    Layout = "~/Areas/Mpa/Views/Layout/_UibLayout2.cshtml";
}
    <!DOCTYPE html>

<html lang="en"> <!-- DO NOT EDIT THIS FILE!! THIS FILE IS MANAGED BY THE CLI! -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body ng-app="{}" >
    <main></main>
    <!--Default Scripts-->
    <script src="{}/app/assets/angular.js"></script>
    <script src="{}/app/assets/ui-router.js"></script>
    <script src="{}/app/assets/jquery.js"></script>
    <script src="{}/app/scripts/app.js"></script>
    <script src="{}/app/scripts/routerConfig.js"></script>
    <!--Injectable scripts-->
    <!--Inject Start-->
    {}
    <!--Inject end--> 
</body>
</html>'''.format(projectName, basePath, basePath, basePath, basePath, basePath, scripts3)
    indexHtml.write(indexFile)
    indexHtml.close()


def yesNo(question, default="yes"):
    valid = {"yes": "yes",   "y": "yes",  "ye": "yes",
             "no": "no",     "n": "no"}
    if default == None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while 1:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return default
        elif choice in valid.keys():
            print(valid[choice])
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def path_to_dict(path):
    d = {'name': os.path.basename(path)}
    if os.path.isdir(path):
        d['type'] = "directory"
        d['children'] = [path_to_dict(os.path.join(path, x)) for x in os.listdir
                         (path)]
    else:
        d['type'] = "file"
    return d


def genDirs(name, ty):
    print('genDirs!')
    if(ty == 'project' or ty == "net"):
        global projectName
        projectName = name
    global directory
    print('Current Dir: ', currentDir)
    directory = os.path.join(currentDir, projectName)
    if not os.path.exists(directory):
        os.makedirs(directory)
    global appDir
    appDir = os.path.join(directory, r'app')
    if(projectExist == 'false'):
        os.makedirs(appDir)
    global componentsDir
    componentsDir = os.path.join(appDir, r'components')
    global serviceDir
    serviceDir = os.path.join(appDir, r'services')
    global valuesDir
    valuesDir = os.path.join(appDir, r'values')
    global constantsDir
    constantsDir = os.path.join(appDir, r'constants')
    global factoriesDir
    factoriesDir = os.path.join(appDir, r'factories')
    global directivesDir
    directivesDir = os.path.join(appDir, r'directives')
    os.chdir(directory)
    global scriptsDir
    scriptsDir = os.path.join(appDir, r'scripts')
    global scriptsPath
    scriptsPath = os.path.join(scriptsDir, r'app.js')
    if(projectExist == 'false'):
        print('false')
        os.makedirs(componentsDir)
        os.makedirs(valuesDir)
        os.makedirs(serviceDir)
        os.makedirs(constantsDir)
        os.makedirs(factoriesDir)
        os.makedirs(directivesDir)
        os.makedirs(scriptsDir)
    else:
        print('true')
    dirsJson = os.path.join(directory, r'dirs.json')
    with open(dirsJson, 'w') as outfile:
        json.dump(path_to_dict(directory), outfile)
    configJson = os.path.join(directory, r'config.json')
    if os.path.isfile(configJson):
        print('Base path set to: ', basePath)
    else:
        configDict = dict([('basePath', basePath), ('other', 1)])
        with open(configJson, 'w') as outfile:
            json.dump(configDict, outfile)

    os.chdir(directory)
    print('current directory: ', os.getcwd())
    if(ty == 'project'):
        genProject(name, ty)
    elif(ty == 'net'):
        genNet(name, ty)        
    elif(ty == 'component'):
        genComponent(name, ty)
    elif(ty == 'service'):
        genService(name, ty)
    elif(ty == 'factory'):
        genFactory(name, ty)
    elif(ty == 'value'):
        genValue(name, ty)
    elif(ty == 'constant'):
        genConstant(name, ty)
    elif(ty == 'directive'):
        genDirective(name, ty)

# def loadDeps():
#   subprocess.check_call('npm init --yes', shell=True)
#   subprocess.check_call('npm install angular@1.6.5 --save', shell=True)
#   subprocess.check_call('npm install --save @uirouter/angularjs', shell=True)


def genConfig(name, ty):
    states = []
    routes = []
    routes2 = ''
    routesJson = os.path.join(appDir, r'routes.json')

    if projectExist == 'true':
        if os.path.isfile(routesJson):
            # print('Hey routesJson found!')
            with open(routesJson, 'r') as f:
                datastore = json.load(f)
                for i in datastore:
                    routes.append(i)
    for i in routableComponents:
        # print(i)
        routes.append(i)
    routessNoDupes = list(set(routes))
    for i in routessNoDupes:
        routes2 = routes2 + i
        states.append(i)
    # print(states)
    routerFile = os.path.join(scriptsDir, r'routerConfig.js')
    configJs = open(routerFile, 'w')
    configJFile = '''/* *** DO NOT EDIT THIS FILE!! THIS FILE IS MANAGED BY THE CLI! *** */
    (function () {{
  "use strict"; 
  var module = angular.module("{}");
      module.config(['$stateProvider','$urlRouterProvider', function($stateProvider, $urlRouterProvider){{
        {}
        $urlRouterProvider.otherwise('/hello');
    }}]); 
  }}()); '''.format(projectName, routes2)
    configJs.write(configJFile)
    configJs.close()
    with open(routesJson, 'w') as outfile:
        json.dump(states, outfile)


def genProject(name, ty):
    # loadDeps()
    fetchDeps()
    # print('genProject!')
    if(ty == 'project'):
        global projectName
        projectName = name
        print('Creating a new ', ty, ' named ', name)
        appJs = open(scriptsPath, 'w')
        appJFile = '''(function () {{
    "use strict"; 
    var {} = angular.module("{}", ['ui.router']); 
    }}()); '''.format(projectName, projectName)
        appJs.write(appJFile)
        appJs.close()
        genComponent('main', 'component')

def genNet(name, ty):
    # loadDeps()
    fetchDeps()
    # print('genProject!')
    if(ty == 'net'):
        global projectName
        projectName = name
        print('Creating a new .', ty, ' project named ', name)
        appJs = open(scriptsPath, 'w')
        appJFile = '''(function () {{
    "use strict"; 
    var {} = angular.module("{}", ['ui.router']); 
    }}()); '''.format(projectName, projectName)
        appJs.write(appJFile)
        appJs.close()
        genComponent('main', 'component')

def checkProject(name, ty):
    print('checkProject!')
    global projectType
    if (ty == 'net'): 
        projectType = 'net'
    else:
        projectType = 'project'
    yN = yesNo(
        'Do you want to choose a path other than the default(default is the current working directory)?')
    if(yN == 'yes'):
        global basePath
        basePath = input('Type your path: ')
        print(basePath)
    global directory
    directory = os.path.join(currentDir, name)
    global dirsJson
    dirsJson = os.path.join(currentDir, r'dirs.json')
    test = find('dirs.json', currentDir)
    if(test):
        print('found: ', test)
        global projectExist
        projectExist = 'true'
        if(ty == 'project' or ty == 'net'):
            print('Project Exist! Exiting!')
            sys.exit(0)
        filename = os.path.join(test, r'dirs.json')
        with open(filename, 'r') as f:
            datastore = json.load(f)
        global projectName
        projectName = datastore['name']
        print("Project name from file: ", projectName)
        genDirs(name, ty)
        return "true"
    else:
        genDirs(name, ty)
        return "false"


def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            print(root)
            return os.path.join(root)


def next():
    yN = yesNo(
        "Do you want to add any services, factories, values or constants?")
    if(yN == 'yes'):
        print(
            '1. Component \n 2. Directive \n 3. Service \n 4. factory \n 5. value \n 6. constant \n ')
        typeOfGen = input(
            'Select one of the options above by typing the number: ')
        if(typeOfGen == '3'):
            name = input('Type the name of your service: ')
            genService(name, 'service')
        elif(typeOfGen == '2'):
            name = input('Type the name of your directive: ')
            genDirective(name, 'directive')
        elif(typeOfGen == '4'):
            name = input('Type the name of your factory: ')
            genFactory(name, 'factory')
        elif(typeOfGen == '5'):
            name = input('Type the name of your value: ')
            genValue(name, 'value')
        elif(typeOfGen == '6'):
            name = input('Type the name of your constant: ')
            genConstant(name, 'constant')
        elif(typeOfGen == '1'):
            name = input('Type the name of your component: ')
            genComponent(name, 'component')
        else:
            print(
                '404 You have reached a part of the CLI that has not be written yet!')
    else:
        sys.exit()

def genComponent(name, ty):
    if(ty == "component"):
        newComponentDir = os.path.join(componentsDir, name)
        os.makedirs(newComponentDir)
        componentPathListing = path_to_dict(componentsDir)
        children = componentPathListing['children']
        #print(children.get("name", "No components yet"))
        print(children)
        generateIndex('components', name)
        print('Creating a new ', ty, ' named: ', name)
        yN = yesNo(
            "Do you want to inject dependencies/services into your componenet? e.g. $http, ...")
        if(yN == "yes"):
            deps = input(
                "Type your dependencies seperated by commas(no spaces!):")
        else:
            deps = ""
        depsArr1 = deps.split(',')
        deps1 = '", "'.join(depsArr1)
        if(yN == 'yes'):
            depsArr = '"' + deps1 + '"'
            comma = ','
        else:
            comma = ''
            depsArr = ""
        componentJsName = name + '.js'
        require = " "
        transclude = " "
        if(name != 'main'):
            yN = yesNo('Do you want to require a parent component?')
            if(yN == "yes"):
                parent = input("Type the name of the parent: ")
                require = '''require: {{
            {}: "^^"
        }}
        '''.format(parent)
                yN = yesNo('Transclude? ')
                if(yN == 'yes'):
                    transclude = 'true'
                else:
                    transclude = 'false'
            else:
                require = " "
        else:
            require = " "
            transclude = 'false'
        scriptsPath = os.path.join(newComponentDir + "\\" + componentJsName)
        js = open(scriptsPath, 'w')
        jsFile = '''(function(){{
    "use strict"; 
    var module = angular.module("{}"); 
    module.controller("{}Ctrl", [{}{} function ({}) {{
        var model = this; 
        model.$onInit = function(){{
        }};
        model.$onChanges = function(){{
        }};
        model.$onDestroy = function(){{
        }};
    }}]);
    module.component("{}", {{
        controller: "{}Ctrl", 
        controllerAs: "model", 
        templateUrl: document.location.protocol + '//' + document.location.host + "{}/app/components/{}/{}.html",
        transclude: {},
        {}
      }});
    }}());'''.format(projectName, name, depsArr, comma, deps, name, name, basePath, name, name, transclude, require)
        js.write(jsFile)
        js.close()
        componentHtmlName = name + '.html'
        if(name == 'main'):
            content = "<ui-view></ui-view>"
        else:
            content = '''<h1>This is the {} componet!</h1>'''.format(name)
        templatePath = os.path.join(newComponentDir + "\\" + componentHtmlName)
        template = open(templatePath, 'w')
        templateFile = '''<!-- This is where you put your template HTML 
      This is the {} component. Put something here! 
    --> 
    {}
    '''.format(name, content)
        template.write(templateFile)
        template.close()
        if(name != 'main'):
            yN = yesNo('Do you want to generate routes for this component?')
            if(yN == "yes"):
                global routableComponents
                state = '''
        var {}State = {{
            name: '{}',
            url: '/{}',
            component: '{}'
        }};
        $stateProvider.state({}State);
        '''.format(name, name, name, name, name)
                routableComponents.append(state)
                genConfig(name, ty)
    yN = yesNo("Do you want to add another component?")
    if(yN == 'yes'):
        name = input("Type the name of your component:")
        genComponent(name, 'component')
    else:
        next()


def genService(name, ty):
    print('Creating a new ', ty, ' named: ', name)
    newServiceDir = os.path.join(serviceDir, name)
    os.makedirs(newServiceDir)
    #servicePathListing = path_to_dict(serviceDir)
    #children = servicePathListing['children']
    generateIndex('services', name)
    serviceJsName = name + '.js'
    scriptsPath = os.path.join(newServiceDir + "\\" + serviceJsName)
    js = open(scriptsPath, 'w')
    jsFile = """(function (){{
    "use strict";
    var module = angular.module("{}");
    module.service('{}',  function (){{
        var serv = {{}};

        return serv;
    }});
  }}());""".format(projectName, name)
    js.write(jsFile)
    js.close()
    next()


def genFactory(name, ty):
    print('Creating a new ', ty, ' named: ', name)
    newFactoryDir = os.path.join(factoriesDir, name)
    print(newFactoryDir)
    os.makedirs(newFactoryDir)
    #factoryPathListing = path_to_dict(factoriesDir)
    yN = yesNo(
        "Do you want to inject dependencies/services into your factory? e.g. $http, ...")
    if(yN == "yes"):
        deps = input("Type your dependencies seperated by commas(no spaces!):")
    else:
        deps = ""
    depsArr1 = deps.split(',')
    deps1 = '", "'.join(depsArr1)
    if(yN == 'yes'):
        depsArr = '"' + deps1 + '"'
        comma = ','
    else:
        comma = ''
        depsArr = ""
    #children = factoryPathListing['children']
    generateIndex('factories', name)
    factoryJsName = name + '.js'
    scriptsPath = os.path.join(newFactoryDir + "\\" + factoryJsName)
    print(scriptsPath)
    js = open(scriptsPath, 'w')
    jsFile = """(function (){{
    "use strict";
    var module = angular.module("{}");
    module.factory('{}', [{}{} function ({}){{
        var serv = {{}};

        return serv;
    }}]);
  }}());""".format(projectName, name, depsArr, comma, deps)
    js.write(jsFile)
    js.close()
    next()


def genValue(name, ty):
    print('Creating a new ', ty, ' named: ', name)
    newValueDir = os.path.join(valuesDir, name)
    print('hello ' + newValueDir)
    os.makedirs(newValueDir)
    #valuePathListing = path_to_dict(valuesDir)
    #children = valuePathListing['children']
    generateIndex('values', name)
    valueJsName = name + '.js'
    scriptsPath = os.path.join(newValueDir + "\\" + valueJsName)
    print(scriptsPath)
    js = open(scriptsPath, 'w')
    jsFile = """(function (){{
    "use strict";
    var module = angular.module("{}");
    module.value('{}', {{ id: "something goes here" }});
  }}());""".format(projectName, name)
    js.write(jsFile)
    js.close()
    next()


def genConstant(name, ty):
    print('Creating a new ', ty, ' named: ', name)
    newConstantDir = os.path.join(constantsDir, name)
    print('hello ' + newConstantDir)
    os.makedirs(newConstantDir)
    #constantPathListing = path_to_dict(constantsDir)
    #children = constantPathListing['children']
    generateIndex('constants', name)
    constantJsName = name + '.js'
    scriptsPath = os.path.join(newConstantDir + "\\" + constantJsName)
    print(scriptsPath)
    js = open(scriptsPath, 'w')
    jsFile = """(function (){{
    "use strict";
    var module = angular.module("{}");
    module.constant('{}', {{ id: "something goes here" }});
  }}());""".format(projectName, name)
    js.write(jsFile)
    js.close()
    next()


def genDirective(name, ty):
    print('Creating a new ', ty, ' named: ', name)
    newDirectiveDir = os.path.join(directivesDir, name)
    print(newDirectiveDir)
    os.makedirs(newDirectiveDir)
    #directivesPathListing = path_to_dict(directivesDir)
    yN = yesNo(
        "Do you want to inject dependencies/services into your directive? e.g. $http, ...")
    if(yN == "yes"):
        deps = input("Type your dependencies seperated by commas(no spaces!):")
    else:
        deps = ""
    depsArr1 = deps.split(',')
    deps1 = '", "'.join(depsArr1)
    if(yN == 'yes'):
        depsArr = '"' + deps1 + '"'
        comma = ','
    else:
        comma = ''
        depsArr = ""
    #children = directivesPathListing['children']
    generateIndex('directives', name)
    directiveJsName = name + '.js'
    scriptsPath = os.path.join(newDirectiveDir + "\\" + directiveJsName)
    print(scriptsPath)
    js = open(scriptsPath, 'w')
    jsFile = """(function (){{
    "use strict";
    var module = angular.module("{}");
    module.directive('{}', [{}{} function ({}){{
        return function (scope, element, attrs) {{
            element.bind("keydown keypress", function (event) {{
                if (event.which === 13) {{
                    scope.$apply(function () {{
                        scope.$eval(attrs.{});
                    }});
                    event.preventDefault();
                }}
            }});
        }};
    }}]);
  }}());""".format(projectName, name, depsArr, comma, deps, name)
    js.write(jsFile)
    js.close()
    next()


parser = argparse.ArgumentParser()
parser.add_argument("-v", "--version", help="Display version information",
                    action="store_true")
parser.add_argument("-n", "--new", help="Generates a new project in current working directory or directory specified.",
                    action="store_true")
parser.add_argument("-N", "--net", help="Generates a new project for .net in current working directory or directory specified.",
                    action="store_true")
parser.add_argument("-c", "--component", help="Generates a new component if project is present.",
                    action="store_true")
parser.add_argument("-s", "--service", help="Generates a new service if project is present.",
                    action="store_true")
parser.add_argument("-f", "--factory", help="Generates a new factory if project is present.",
                    action="store_true")
parser.add_argument("-V", "--value", help="Generates a new value if project is present.",
                    action="store_true")
parser.add_argument("-C", "--constant", help="Generates a new constant if project is present.",
                    action="store_true")
parser.add_argument("-d", "--directive", help="Generates a new directive if project is present.",
                    action="store_true")
parser.add_argument("name")

args = parser.parse_args()
if args.version:
    print('ang-gen Version =', version)
elif args.new:
    if args.name:
        checkProject(args.name, "project")
elif args.net:
    if args.name:
        checkProject(args.name, "net")
elif args.component:
    if args.name:
        checkProject(args.name, "component")
elif args.service:
    if args.name:
        checkProject(args.name, "service")
elif args.factory:
    if args.name:
        checkProject(args.name, "factory")
elif args.value:
    if args.name:
        checkProject(args.name, "value")
elif args.constant:
    if args.name:
        checkProject(args.name, "constant")
elif args.directive:
    if args.name:
        checkProject(args.name, "directive")
